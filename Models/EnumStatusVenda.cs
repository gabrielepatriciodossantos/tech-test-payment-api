namespace TrilhaApiDesafio.Models
{
    public enum EnumStatusVenda
    {
        AguardandoPagamento,
        PagamentoAprovado,
        Cancelada,
        Enviadoparatransportadora,
        Entregue
    }
}