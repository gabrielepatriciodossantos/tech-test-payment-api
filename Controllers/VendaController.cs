using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TrilhaApiDesafio.Context;
using TrilhaApiDesafio.Models;

namespace TrilhaApiDesafio.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        private readonly OrganizadorContext _context;

        public VendaController(OrganizadorContext context)
        {
            _context = context;
        }

        //Buscar por Id da Venda
        [HttpGet("{id}")]
        public IActionResult ObterPorId(int id)
        {
            Venda venda = _context.vendas.Include(u => u.Vendedor).FirstOrDefault(u => u.Id == id);

            if (venda == null)
               return NotFound();

            return Ok(venda);
        }

        //Buscar pelo Status
        [HttpGet("ObterPorStatus")]
        public IActionResult ObterPorStatus(EnumStatusVenda status)
        {
            var Venda = _context.vendas.Where(x => x.Status == status);
            return Ok(Venda);
        }

        //Registro de Venda
        [HttpPost("CriarVenda")]
        public IActionResult Criar(Venda venda)
        {
            if (venda.Data == DateTime.MinValue)
                return BadRequest(new { Erro = "A data da tarefa não pode ser vazia" });

            if (venda.Itensvendidos == "")
               return NotFound();            

          if(_context.vendedor.Find(venda.Vendedor.Id) != null)         
                venda.Vendedor = _context.vendedor.Find(venda.Vendedor.Id);
          else
            return BadRequest(new { Erro = "Vendedor não encontrado" });


            _context.Add(venda);
          _context.SaveChanges();
          return CreatedAtAction(nameof(ObterPorId), new { id = venda.Id }, venda);
        }

        //Registro de Vendedor
        [HttpPost("CriarVendedor")]
        public IActionResult CriarVendedor(Vendedor vendedor)
        {
          
            _context.Add(vendedor);
            _context.SaveChanges();
            return CreatedAtAction(nameof(ObterPorId), new { id = vendedor.Id }, vendedor);
        }

        //Atualizar Status
        [HttpPut("{id}")]
        public IActionResult Atualizar(int id, Venda venda)
        {
            var vendaBanco = _context.vendas.Find(id);
            bool atualizado = false;

            if (vendaBanco == null)
                return NotFound();

            if(vendaBanco.Status == EnumStatusVenda.AguardandoPagamento)
                    if(venda.Status == EnumStatusVenda.Cancelada || venda.Status == EnumStatusVenda.PagamentoAprovado)
                {
                    vendaBanco.Status = venda.Status;
                    _context.Update(vendaBanco);
                    _context.SaveChanges();
                    atualizado = true;
                }
                    

            if (vendaBanco.Status == EnumStatusVenda.PagamentoAprovado)
                    if(venda.Status == EnumStatusVenda.Enviadoparatransportadora || venda.Status == EnumStatusVenda.Cancelada)
                {
                    vendaBanco.Status = venda.Status;
                    _context.Update(vendaBanco);
                    _context.SaveChanges();
                    atualizado = true;
                }

            if (vendaBanco.Status == EnumStatusVenda.Enviadoparatransportadora)
                    if(venda.Status == EnumStatusVenda.Entregue)
                {
                    vendaBanco.Status = venda.Status;
                    _context.Update(vendaBanco);
                    _context.SaveChanges();
                    atualizado = true;
                }

            vendaBanco = _context.vendas.Include(u => u.Vendedor).FirstOrDefault(u => u.Id == id);

            if(atualizado == false)
                return BadRequest("Atualização de status invalida");

            return Ok(vendaBanco);
        }

      
    }
}
